package gr.project.future.team1.domain;

import gr.project.future.team1.enums.RepairStatus;
import gr.project.future.team1.enums.RepairType;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "REPAIR")
public class Repair {

    @Id
    @Column(name = "Id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "Date", nullable = false)
    private LocalDate date;

    @Column(name = "Status")
    private RepairStatus status;

    @Column(name = "Type")
    private RepairType type;

    @Column(name = "Cost")
    private long cost;

    @Column(name = "Description", length = 120)
    private String description;

    @ManyToOne(optional = false)
    @JoinColumn(name = "User_Id", referencedColumnName = "Id")
    private User user;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public RepairStatus getStatus() {
        return status;
    }

    public void setStatus(RepairStatus status) {
        this.status = status;
    }

    public RepairType getType() {
        return type;
    }

    public void setType(RepairType type) {
        this.type = type;
    }

    public long getCost() {
        return cost;
    }

    public void setCost(long cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Repair(Long id, LocalDate date, RepairStatus status, RepairType type, long cost, String description, User user) {
        this.id = id;
        this.date = date;
        this.status = status;
        this.type = type;
        this.cost = cost;
        this.description = description;
        this.user = user;
    }

    public Repair() {}
}
