package gr.project.future.team1.validator;

import gr.project.future.team1.form.RepairSearchForm;
import gr.project.future.team1.utils.GlobalAttributes;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
@Component
public class RepairSearchFormValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return RepairSearchForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        RepairSearchForm repairSearchForm=(RepairSearchForm) target;
        if(!repairSearchForm.getAfm().equals("") &&(!repairSearchForm.getAfm().matches(GlobalAttributes.AFM_PATTERN))){
            errors.rejectValue("afm","AFM_PATTERN_MESSAGE", GlobalAttributes.AFM_PATTERN_MESSAGE);
        }
        if(!repairSearchForm.getVehiclePlate().equals("") && (!repairSearchForm.getVehiclePlate().matches(GlobalAttributes.PLATE_PATTERN))){
            errors.rejectValue("vehiclePlate","PLATE_PATTERN_MESSAGE", GlobalAttributes.PLATE_PATTERN_MESSAGE);
        }
        if(repairSearchForm.getDateFrom() != null && repairSearchForm.getDateTo() != null) {
            if(repairSearchForm.getDateFrom().isAfter(repairSearchForm.getDateTo())) {
                errors.rejectValue("dateTo","DATE_FROM_MESSAGE", GlobalAttributes.DATE_FROM_MESSAGE);
                errors.rejectValue("dateFrom","DATE_FROM_MESSAGE", GlobalAttributes.DATE_FROM_MESSAGE);
            }
        }

    }
}
