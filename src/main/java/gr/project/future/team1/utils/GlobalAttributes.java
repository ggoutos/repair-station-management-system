package gr.project.future.team1.utils;

public class GlobalAttributes {
    public static final String EMPTY_STRING = "";
    public static final String ERROR_MESSAGE = "errorMessage";
    public static final String MESSAGE = "message";
    public static final String REDIRECT_MESSAGE = "redirectMsg";
    public static final String MESSAGE_LIST = "messages";
    public static final String TIMESTAMP_COOKIE_NAME = "TIMESTAMP";

    /* patterns */
    public static final String AFM_PATTERN = "^[0-9]{9}$";
    public static final int AFM_LENGTH = 9;
    public static final String NAME_SURNAME_PATTERN = "^[A-Za-z]*$";
    public static final int NAME_SURNAME_SIZE = 2;
    public static final String MAIL_PATTERN = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,63}$";
    public static final int PASSWORD_SIZE = 3;
    public static final String BRAND_PATTERN = "^[A-Za-z0-9]*$";
    public static final int BRAND_SIZE = 2;
    public static final String PLATE_PATTERN = "^[a-zA-Z]{3}+[0-9]{3,4}$";
    public static final int ADDRESS_SIZE = 3;
    public static final String DATE_PATTERN = "yyyy-MM-dd";

    /* messages */
    public static final String DATE_MESSAGE = "Please choose a Date";
    public static final String DESCRIPTION_MESSAGE = "Description must be at least 5 characters long";
    public static final String USER_MESSAGE = "User cannot be empty";
    public static final String AFM_PATTERN_MESSAGE = "AFM must contain only numbers";
    public static final String AFM_LENGTH_MESSAGE = "AFM requires 9 digits";
    public static final String NAME_SURNAME_PATTERN_MESSAGE = "Please use only letters";
    public static final String NAME_SURNAME_SIZE_MESSAGE = "At least 2 letters required";
    public static final String ADDRESS_SIZE_MESSAGE = "Address has to be at least 3 numbers or letters";
    public static final String MAIL_PATTERN_MESSAGE = "Please use a valid email";
    public static final String PASSWORD_SIZE_MESSAGE = "Password must be larger than 3";
    public static final String BRAND_PATTERN_MESSAGE = "Brand can only be letters and numbers";
    public static final String BRAND_SIZE_MESSAGE = "Minimum Brand size 2";
    public static final String PLATE_PATTERN_MESSAGE = "Plate required 3 chars 3 to 4 digits e.g ABC1234";
    public static final String DATE_FROM_MESSAGE = "Date From cannot be After Date to";
    public static final String COST_MESSAGE = "Use a number in the cost field";
    public static final String USER_AFM_EXISTS = "User with this Afm already exists";
    public static final String USER_MAIL_EXISTS = "User with this Email already exists";
    public static final String AFM_EMAIL_USED_MESSAGE = "The email and Afm belong to another user";
}
