package gr.project.future.team1.service;

import gr.project.future.team1.domain.User;
import gr.project.future.team1.form.RegisterUserForm;
import gr.project.future.team1.model.UserModel;
import gr.project.future.team1.model.UserRegisterModel;

import java.util.List;

public interface UserService {
    List<UserModel> findAll();
    void create(UserRegisterModel userRegisterModel);
    List<User> getUser(String afm, String email);
    User getUserById(long id);
    void deleteUser(long id);
    void userUpdate(RegisterUserForm form);

}
