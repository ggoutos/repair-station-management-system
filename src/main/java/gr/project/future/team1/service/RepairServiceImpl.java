package gr.project.future.team1.service;

import gr.project.future.team1.domain.Repair;
import gr.project.future.team1.domain.User;
import gr.project.future.team1.form.RegisterRepairForm;
import gr.project.future.team1.form.RepairSearchForm;
import gr.project.future.team1.repository.RepairRepository;
import gr.project.future.team1.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RepairServiceImpl implements RepairService {

    @Autowired
    RepairRepository repairRepository;

    @Autowired
    UserRepository userRepository;

    @Override
    public List<Repair> findAll() {
        return repairRepository.findAll();
    }

    @Override
    public Repair findById(long repairId) {
        return repairRepository.findById(repairId);
    }

    @Override
    public List<Repair> findByCriteria(RepairSearchForm repairSearchForm) {

        if (repairSearchForm.allDatesFrom()) {
            repairSearchForm.setDateTo(LocalDate.of(9999, 12, 31));
        }
        if (repairSearchForm.allDatesTo()) {
            repairSearchForm.setDateFrom(LocalDate.of(0001, 01, 01));
        }

        return repairRepository.findAll().stream()
                //Each filter checks if condition is true otherwise doesn't apply to the stream.

                //dateFrom <= Date <= dateTo
                .filter(repairSearchForm.allDatesBetween()
                        ? repair -> ((repair.getDate().compareTo(repairSearchForm.getDateFrom()) >= 0) && (repair.getDate().compareTo(repairSearchForm.getDateTo()) <= 0))
                        : repair -> true)
                //search by user afm
                .filter(repair -> repair.getUser().getAfm().contains(repairSearchForm.getAfm().strip()))
                //search by vehicle plate
                .filter(repair -> repair.getUser().getVehiclePlate().contains(repairSearchForm.getVehiclePlate().strip()))
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(long repairId) {
        repairRepository.deleteById(repairId);
    }

    @Override
    public void create(RegisterRepairForm registerRepairForm) {
        User user = userRepository.findByAfm(registerRepairForm.getUser());
        Repair repair = new Repair(registerRepairForm.getId(), registerRepairForm.getDate(), registerRepairForm.getStatus(), registerRepairForm.getType(), Long.parseLong(registerRepairForm.getCost()), registerRepairForm.getDescription(), user);
        repairRepository.save(repair);
    }

    @Override
    public List<Repair> getRepairsByUserEmail(String email) {
        List<Repair> repairsList = repairRepository.findAll().stream()
                .filter(repair ->repair.getUser().getEmail().equals(email))
                .collect(Collectors.toList());
        return repairsList;
    }

    @Override
    public List<Repair> getTodayRepairs() {
        LocalDate today = LocalDate.now();
        List<Repair> todayRepairs = repairRepository.findByDate(today);
        return todayRepairs;
    }

}
