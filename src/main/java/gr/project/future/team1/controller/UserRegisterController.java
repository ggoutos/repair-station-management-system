package gr.project.future.team1.controller;

import gr.project.future.team1.controller.mapper.RegisterUserFormToUserRegisterModelMapper;
import gr.project.future.team1.enums.UserType;
import gr.project.future.team1.exception.UserAfmExistsException;
import gr.project.future.team1.exception.UserEmailExistException;
import gr.project.future.team1.form.RegisterUserForm;
import gr.project.future.team1.model.UserRegisterModel;
import gr.project.future.team1.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class UserRegisterController {
    private static final String REGISTER_USER_FORM = "registerUserForm";

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private RegisterUserFormToUserRegisterModelMapper registerUserFormToUserRegisterModelMapper;

    @GetMapping(value = "/users/registration")
    public String register(Model model) {
        if (!model.containsAttribute(REGISTER_USER_FORM)) {
            RegisterUserForm registerUserForm = new RegisterUserForm();
            registerUserForm.setUserType(UserType.OWNER);
            model.addAttribute(REGISTER_USER_FORM, registerUserForm);
        }
        return "user-registration";
    }

    @PostMapping(value = "/users/registration")
    public String register(Model model, @Valid RegisterUserForm registerUserForm,
                           BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "user-registration";
        }
        UserRegisterModel userRegisterModel = registerUserFormToUserRegisterModelMapper.mapToUserModel(registerUserForm);
        try {
            userService.create(userRegisterModel);
        } catch (UserAfmExistsException e) {
            model.addAttribute("errorInAfm", e.getMessage());
            return "user-registration";
        } catch (UserEmailExistException e) {
            model.addAttribute("errorInEmail", e.getMessage());
            return "user-registration";
        }
        return "redirect:/";
    }


}
