package gr.project.future.team1.controller;

import gr.project.future.team1.domain.Repair;
import gr.project.future.team1.domain.User;
import gr.project.future.team1.service.RepairService;
import gr.project.future.team1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;
import java.util.List;

@Controller
public class IndexController {

    @Autowired
    RepairService repairService;

    @Autowired
    UserService userService;

    @GetMapping("/")
    public String index(Model model) {
        List<Repair> todayRepairs = repairService.getTodayRepairs();
        model.addAttribute("todayRepairs", todayRepairs);
        return "index";
    }

    @GetMapping(value = "/owner")
    public String index(Model model, Principal principal) {
        String userEmail = principal.getName();
        List<Repair> userRepairs = repairService.getRepairsByUserEmail(userEmail);
        model.addAttribute("userRepairs", userRepairs);
        return "owner-index";
    }

    @GetMapping("/account")
    public String index(Model model, Authentication authentication) {
        List<User> users = userService.getUser("", authentication.getName());
        model.addAttribute("user", users.get(0));
        return "account";
    }

}
