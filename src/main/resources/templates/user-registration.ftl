<!doctype html>
<html lang="en">
<head>
    <#include "partials/head.ftl">
    <title>Car Repair - User Registration</title>
</head>

<body>


<main role="main" class="container">

    <#include "partials/navbar.ftl">

    <div class="jumbotron" style="padding: 4rem 2rem">
        <h1 style="text-align: center">User Registration</h1>


        <#import "/spring.ftl" as spring />
        <form id="registerUserForm" onsubmit="this.validate()" action="/users/registration" name="registerUserForm" method="post">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputName">Name</label>
                    <@spring.bind "registerUserForm.name"/>
                    <input type="text" class="form-control" id="inputName" placeholder="Name" name="name" value="${registerUserForm.name!''}">
                    <#list spring.status.errorMessages as error>
                    <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputSurname">Surname</label>
                    <@spring.bind "registerUserForm.surname"/>
                    <input type="text" class="form-control" value="${registerUserForm.surname!''}" id="inputSurname" placeholder="Surname" name="surname">
                    <#list spring.status.errorMessages as error>
                    <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputAFM">AFM</label>
                    <@spring.bind "registerUserForm.afm"/>
                    <input type="text" class="form-control" value="${registerUserForm.afm!''}" id="inputAFM" placeholder="AFM" name="afm">
                    <#list spring.status.errorMessages as error>
                    <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                    <div>
                    <#if errorInAfm??>
                        <div class="alert alert-danger" role="alert"><span>${errorInAfm}</span></div>
                    </#if>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputAddress">Address</label>
                    <@spring.bind "registerUserForm.address"/>
                    <input type="text" class="form-control" value="${registerUserForm.address!''}" id="inputAddress" placeholder="Address" name="address">
                    <#list spring.status.errorMessages as error>
                    <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail">Email</label>
                    <@spring.bind "registerUserForm.email"/>
                    <input type="text" class="form-control" value="${registerUserForm.email!''}" id="inputEmail" placeholder="Email" name="email">
                    <#list spring.status.errorMessages as error>
                    <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                    <div>
                    <#if errorInEmail??>
                        <div class="alert alert-danger" role="alert"><span>${errorInEmail}</span></div>
                    </#if>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputPassword">Password</label>
                    <@spring.bind "registerUserForm.password"/>
                    <input type="password" class="form-control" id="inputPassword" placeholder="Password" name="password">
                    <#list spring.status.errorMessages as error>
                    <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputVehicleBrand">Vehicle Brand</label>
                    <@spring.bind "registerUserForm.vehicleBrand"/>
                    <input type="text" class="form-control" value="${registerUserForm.vehicleBrand!''}" id="inputVehicleBrand" placeholder="Vehicle Brand" name="vehicleBrand">
                    <#list spring.status.errorMessages as error>
                    <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputVehiclePlate">Vehicle Plate</label>
                    <@spring.bind "registerUserForm.vehiclePlate"/>
                    <input type="text" class="form-control" value="${registerUserForm.vehiclePlate!''}" id="inputVehiclePlate" placeholder="Vehicle Plate" name="vehiclePlate">
                    <#list spring.status.errorMessages as error>
                    <div class="alert alert-danger" role="alert">${error}</div>
                    </#list>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="inputUserType">User Type</label>
                    <select id="inputUserType" class="form-control" name="userType">
                    <#if '${registerUserForm.userType}' == 'ADMIN'>
                        <option selected>ADMIN</option>
                        <option>OWNER</option>
                    <#else>
                        <option>ADMIN</option>
                        <option selected>OWNER</option>
                    </#if>
                    </select>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Register</button>
        </form>

    </div>
</main>

<#include "partials/scripts.ftl">


</body>
</html>
