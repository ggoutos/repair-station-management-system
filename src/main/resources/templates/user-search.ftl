<!doctype html>
<html lang="en">
<head>
    <#include "partials/head.ftl">
    <title>Car Repair - User Search</title>
</head>

<body>


<main role="main" class="container">

    <#include "partials/navbar.ftl">

    <div class="jumbotron" style="padding: 4rem 2rem">
        <h1 style="text-align: center">User Search</h1>

        <#import "/spring.ftl" as spring />
        <form action="/users/search" name="userSearchForm" method="post">
            <h3>Search criteria:</h3>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputAFM">AFM</label>
                    <@spring.bind "userSearchForm.afm"/>
                    <input type="text" name = "afm" value="${userSearchForm.afm!''}"class="form-control" id="inputAFM" placeholder="AFM">
                    <#list spring.status.errors.getFieldErrors("afm") as error>
                    <div class="alert alert-danger" role="alert">${error.defaultMessage}</div>
                    </#list>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail">Email</label>
                    <@spring.bind "userSearchForm.email"/>
                    <input type="text" name = "email" value="${userSearchForm.email!''}"class="form-control" id="inputEmail" placeholder="Email">
                    <#list spring.status.errors.getFieldErrors("email") as error>
                        <div class="alert alert-danger" role="alert">${error.defaultMessage}</div>
                    </#list>
                </div>
            </div>


            <button type="submit" class="btn btn-primary">Search</button>
        </form>

    </div>
        <p class="errorMsg" style="color: red">
        <strong>${errorMessage!''}</strong>
        </p>

</main>


<#include "partials/scripts.ftl">

</body>
</html>
