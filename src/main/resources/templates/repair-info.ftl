<!doctype html>
<html lang="en">
<head>
    <#include "partials/head.ftl">
    <title>Car Repair - Repair Info</title>
</head>

<body>

    <#include "partials/navbar.ftl">

    <div class="jumbotron" style="padding: 4rem 2rem">
        <h1 style="text-align: center">Repair Info</h1>

        <table class="table">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Description</th>
                    <th>Status</th>
                    <th>Type</th>
                    <th>Cost</th>
                    <th>User Surname</th>
                    <th>User Plate</th>
                </tr>
            </thead>
            <tbody>
                <#list repairs as repair>
                    <tr>
                        <td> ${repair.date}</td>
                        <td> ${repair.description}</td>
                        <td> ${repair.status}</td>
                        <td> ${repair.type}</td>
                    <td style="text-align: right"> ${repair.cost} €</td>
                        <td> ${repair.user.surname}</td>
                        <td> ${repair.user.vehiclePlate}</td>

                        <td class="text-right">
                            <a class="btn btn-danger button-delete-confirmation"
                               onclick="return confirm('Do you really want to delete this repair?')"
                               type="submit" href="/repairs/delete/${repair.id}">Delete</a>
                            <a class="btn btn-success"
                               type="submit" href="/repairs/update/${repair.id}">Update</a>
                        </td>
                    </tr>
                </#list>
            </tbody>
        </table>

    </div>


<#include "partials/scripts.ftl">
</body>
</html>
