
    <!doctype html>
    <html lang="en">
    <head>
        <#include "partials/head.ftl">
        <#include "partials/scripts.ftl">
        <title>Car Repair - User Info</title>

</head>

<body>

<main role="main" class="container">

    <#include "partials/navbar.ftl">

    <div class="jumbotron" style="padding: 4rem 2rem">
        <h1 style="text-align: center">User Info</h1>

        <table class="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Surname</th>
                    <th>Afm</th>
                    <th>Email</th>
                    <th>Adress</th>
                    <th>Role</th>
                    <th>Vehicle brand</th>
                    <th>Plate</th>
                </tr>
            </thead>
            <tbody>
            <#list users as user>
                <tr>
                    <td> ${user.name!'-'}</td>
                    <td> ${user.surname!'-'}</td>
                    <td> ${user.afm!'-'}</td>
                    <td>${user.email!'-'}</td>
                    <td> ${user.address!'-'}</td>
                    <td> ${user.userType!'-'}</td>
                    <td> ${user.vehicleBrand!'-'}</td>
                    <td>${user.vehiclePlate!'-'}</td>
                    <td class="text-right">
                        <a class="btn btn-danger button-delete-confirmation"
                           onclick="return confirm('Do you really want to delete this user?')"
                           type="submit" href="/users/delete/${user.id}">Delete</a>
                        <a class="btn btn-success"
                           type="submit" value="Edit" href="/users/update/${user.id}">Update</a>
                    </td>
                </tr>
            </#list>
            </tbody>
        </table>

    </div>
</main>

</body>